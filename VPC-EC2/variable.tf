# VPC Variables
variable "region" {
  default       = "us-east-1"
  description   = "AWS Region"
  type          = string
}

variable "vpc-cidr" {
  default       = "10.0.0.0/16"
  description   = "VPC CIDR Block"
  type          = string
}

variable "public-subnet-1-cidr" {
  default       = "10.0.0.0/24"
  description   = "Public Subnet 1 CIDR Block"
  type          = string
}

variable "private-subnet-1-cidr" {
  default       = "10.0.2.0/24"
  description   = "Private Subnet 1 CIDR Block"
  type          = string
}

variable "awsprops" {
    default = {
    region = "us-east-1"
    ami = "ami-0ed9277fb7eb570c9"
    itype = "t2.micro"
    publicip = true
    keyname = "NVDPT3"
    secgroupname = "patona-Sec-Group"
    }
}